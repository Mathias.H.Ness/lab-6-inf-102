package INF102.lab6.cheapFlights;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import INF102.lab6.graph.DirectedEdge;
import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> graph = new WeightedDirectedGraph<>();

        for (Flight flight : flights) {
            graph.addVertex(flight.start);
            graph.addVertex(flight.destination);
            graph.addEdge(flight.start, flight.destination, flight.cost);
        }

        return graph;
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        return bellmanFord(flights, start, destination, nMaxStops);
    }

    private int bellmanFord(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);
        HashMap<City, Integer> priceMap = new HashMap<>();
        
        for (City city : graph.vertices()) {
            priceMap.put(city, Integer.MAX_VALUE);
        }
        
        priceMap.put(start, 0);
    
        for (int i = 0; i <= nMaxStops; i++) {
            HashMap<City, Integer> newpriceMap = new HashMap<>(priceMap);
            for (Flight flight : flights) {

                City startCity = flight.start;
                City destCity = flight.destination;
                long cost = flight.cost;
    
                long newCost = priceMap.get(startCity) + cost;
                if (newCost < newpriceMap.get(destCity)) {
                    newpriceMap.put(destCity, (int)newCost);
                }
            }
            priceMap = newpriceMap;
        }

        return priceMap.get(destination);
    }

}
